import api from '../../api/products.js';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    cartProducts: [],
    currencyName: 'VGTB',
  },
  getters: {
    GET_CART_PRODUCTS(state) {
      return state.cartProducts;
    },
    GET_CURRENCY_NAME(state) {
      return state.currencyName;
    },
    GET_CART_PRICE(state) {
      let value = 0;

      state.cartProducts.forEach((item) => {
        value = value + (item.price * item.amount);
      });

      return value;
    },
  },
  mutations: {
    UPDATE_CART_PRODUCT(state, data) {
      state.cartProducts = data;
    },
    ADD_CART_PRODUCT(state, data) {
      const foundedItem = state.cartProducts.find(item => item.id === data.id);

      if (!foundedItem) {
        state.cartProducts.push(data);
        return;
      }

      state.cartProducts.forEach(item => {
        if (item.id !== data.id) {
          return;
        }

        item.amount = item.amount + data.amount;
      });
    },
  },
  actions: {
    API_GET_PRODUCTS_LIST() {
      return api.getProductsList();
    },
  },
})
